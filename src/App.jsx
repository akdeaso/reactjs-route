import { Link, Outlet } from "react-router-dom";
import "./App.css";

function App() {
  return (
    <div className="App">
      <h2>Belajar React Routing</h2>
      <nav>
        <Link to={"/home"} className="mr">
          Home
        </Link>
        <Link to={"/about"} className="mr">
          About
        </Link>
        <Link to={"/tag/otomotif"} className="mr">
          Otomotif
        </Link>
        <Link to={"/tag/teknologi"} className="mr">
          Teknologi
        </Link>
      </nav>
      <Outlet />
    </div>
  );
}

export default App;
