import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "../components/Home.jsx";
import About from "../components/About.jsx";
import Tags from "../components/Tags.jsx";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />}>
          <Route path="/home" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/tag/:name" element={<Tags />} />
        </Route>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>
);
