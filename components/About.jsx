import { useNavigate } from "react-router-dom";

const About = () => {
  const navigate = useNavigate();
  const handleClick = () => {
    navigate("../home");
  };
  return (
    <>
      <h3>Selamat datang di About</h3>
      <p>
        Halaman "Tentang Kami" (About) adalah tempat di mana kita dapat berbagi
        informasi mengenai tujuan, visi, misi, dan nilai-nilai inti dari suatu
        aplikasi atau proyek. Ini memberikan pengguna wawasan lebih dalam
        tentang latar belakang, tim di balik pengembangan, serta pandangan yang
        menggerakkan proyek tersebut. Pada halaman ini, kita dapat memaparkan
        cerita unik yang mendasari pengembangan aplikasi, menyoroti pencapaian,
        dan menjelaskan bagaimana proyek tersebut memberikan manfaat bagi
        pengguna. Halaman "Tentang Kami" juga dapat memainkan peran penting
        dalam membangun kepercayaan dan koneksi emosional dengan pengguna,
        karena memberikan mereka pandangan lebih mendalam tentang identitas dan
        komitmen dari proyek yang mereka gunakan.
      </p>
      <button onClick={handleClick}>Kembali ke Home</button>
    </>
  );
};

export default About;
