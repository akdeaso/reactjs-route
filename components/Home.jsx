const Home = () => {
  return (
    <>
      <h3>Selamat datang di Home</h3>
      <p>
        Belajar routing dalam React adalah langkah penting dalam memahami cara
        mengelola navigasi antara berbagai halaman atau komponen dalam aplikasi
        web. Dengan menggunakan pustaka seperti React Router, kita dapat
        menciptakan pengalaman pengguna yang mulus dan dinamis. Melalui konsep
        ini, kita dapat menghubungkan berbagai komponen aplikasi dengan URL yang
        terkait, memungkinkan pengguna berpindah antara konten tanpa harus
        melakukan refresh halaman. Pemahaman tentang routing memungkinkan kita
        mengatur rute, parameter, dan navigasi berdasarkan tindakan pengguna
        atau perubahan kondisi aplikasi. Ini adalah keterampilan penting dalam
        pengembangan web dengan React, yang memungkinkan kita untuk menciptakan
        aplikasi yang responsif, user-friendly, dan memiliki struktur yang
        terorganisir dengan baik.
      </p>
    </>
  );
};

export default Home;
