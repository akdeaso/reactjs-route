import { useNavigate, useParams } from "react-router-dom";

const Tags = () => {
  const navigate = useNavigate();
  const params = useParams();
  const handleHome = () => {
    navigate("../home");
  };
  const handleAbout = () => {
    navigate("../about");
  };
  return (
    <>
      <h3>Selamat datang di tag {params.name} </h3>
      <p>
        Halaman "Tag {params.name}" adalah tempat yang berfungsi untuk
        mengelompokkan dan mengorganisir konten dalam aplikasi atau situs web
        berdasarkan kategori atau topik tertentu. Ini memungkinkan pengguna
        untuk dengan mudah menavigasi dan menemukan konten yang relevan dengan
        minat atau kebutuhan mereka. Dengan mengklik atau memilih tag tertentu,
        pengguna dapat mengakses berbagai konten yang terkait dengan tag
        tersebut, yang membantu mereka untuk mengeksplorasi informasi lebih
        mendalam atau beragam. Halaman "Tag {params.name}" berperan penting
        dalam meningkatkan kemudahan navigasi, meningkatkan pengalaman pengguna,
        dan memfasilitasi aksesibilitas konten dengan cara yang terstruktur dan
        terorganisir.
      </p>
      <button onClick={handleHome} className="mr">
        Kembali ke Home
      </button>
      <button onClick={handleAbout}>Menuju ke About</button>
    </>
  );
};

export default Tags;
